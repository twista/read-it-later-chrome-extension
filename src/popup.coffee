###*
 * read it later
 * chrome extension
 * @author  Michal Haták <me@twista.cz>
###


###*
 * helper - support for .remove()
###

Element::remove = () ->
    @parentElement.removeChild @

NodeList::remove = HTMLCollection::remove = () ->
    @parentElement.removeChild @[i] for i in @length

###*
 * dynamic element builder
 * @param  {string} name   element name, e.g "a"
 * @param  {Object} params attributes
 * @return {Object} element
###
buildElement = (name, params = {}) ->
    elem = document.createElement(name)
    for key, value of params
        elem.setAttribute key, value
    elem


clear = (cb) ->
    document.getElementById("result-table").remove() if document.getElementById "result-table"
    if (cb && typeof(cb) == "function")
        cb()
    return


###*
 * get hostname by url
 * @param  {string} url
 * @return {string}
###
getHost = (url) ->
    link = document.createElement 'a'
    link.href = url;
    return link.hostname;

###*
 * get favicon url
 * @param  {string} url
 * @return {string}
###
getFavicon = (url) =>
    return 'http://www.google.com/s2/favicons?domain=' + getHost(url)


xpath = (expression, node)->
    document.evaluate(expression, node, null, XPathResult.ANY_UNORDERED_NODE_TYPE).singleNodeValue

###*
 * render items
 *
 * tempate:
 *
 * <table id="template">
 *     <tr>
 *         <td class="favicon"><img></td>
 *         <td class="name"><a target="_blank"></a></td>
 *         <td class="goto"><a href="#" target="_blank"><img /></a></td>
 *         <td class="delete"><a href="#" class="del-item"><img /></a></td>
 *     </tr>
 * </table>
 *
 * @param  {Object} root document DOM
 * @return {Object}
###
render = (data) =>

    ## template definition
    result   = document.getElementById 'results'
    template = xpath('//table[@id="template"]//tr', document);
    favicon  = xpath('//td[@class="favicon"]/img', template);
    name     = xpath('//td[@class="name"]/a', template);
    goto     = xpath('//td[@class="goto"]/a', template);
    goto_img = xpath('//td[@class="goto"]/a/img', template);
    del      = xpath('//td[@class="delete"]/a', template);
    del_img  = xpath('//td[@class="delete"]/a/img', template);

    for key, item of data
        favicon.src = getFavicon(item.url)

        name.href = item.url
        name.title = chrome.i18n.getMessage "otevrit_v_okne" + " :: " + item.name
        name.innerHTML = item.name.substring 0,50
        name.innerHTML += '...' if item.name.length > 50

        goto.href = item.url
        goto_img.src = chrome.extension.getURL 'img/go.png'

        del.rel = key
        del_img.src = chrome.extension.getURL 'img/close.png'

        fragment = template.cloneNode(true);
        result.appendChild(fragment);

    # end-for

    # create add-item button
    add_item = buildElement "a", {class: "add_link" }
    add_item_img = buildElement "img", {src: chrome.extension.getURL('img/add.png')}

    add_item.appendChild add_item_img
    add_item.innerHTML += chrome.i18n.getMessage("pridat");

    add_item.addEventListener "click", () =>
        chrome.runtime.sendMessage {message:'addItem'}, (response) ->
            reload()
        return
    document.body.appendChild add_item

    # add delete event listeners - send message to background script
    for item in document.getElementsByClassName 'del-item'
        console.log item
        item.addEventListener "click", (e) ->
                console.log @rel
                chrome.runtime.sendMessage {message:'removeItem', itemkey: @rel}, (response) ->
                    console.log(response)
                    reload()


    return


reload = ()->
    window.location.reload()

###*
 * register onload event
 * @return {Object}
###
window.onload = ()->
    console.log "onload -> send request";
    chrome.runtime.sendMessage {message:'getItems'}, (response) ->
        console.log(response)
        if response.items
            render response.items

