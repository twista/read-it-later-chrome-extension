#!/bin/sh

rm -rf release/build.zip

coffee -o build src

cd build

zip -r ../release/build.zip ./*
